#!/usr/bin/env python
"""
A library to get the info on a given Ventrilo server

Author: Joshua Milas
Python: 3.4 & 2.7
Platform: Windows & Linux
Version: 0.1
Last Modified: 2014-08-05
"""

import subprocess
import re
import sys
import os
import time


class VentriloServerInfo():
    PATH_TO_STATUS = 'ventrilo_status'

    def __init__(self, server, verbose=0, path_to_program=PATH_TO_STATUS):
        self.PATH_TO_STATUS = path_to_program
        self._get_correct_status_path()
        self.server = server
        self.is_server_alive = self.check_if_server_is_valid()
        self.verbose = verbose
        self.status = {}
        return
##----------- Class functions ------------
    def _get_correct_status_path(self):
        """
        Set the correct path to ventrilo_status

        :rtype None
        :return: None
        """
        devnull = open(os.devnull, 'w')  # The null device so we do not print to the console

        # The status program
        try:
            subprocess.call([self.PATH_TO_STATUS], stdout=devnull, stderr=devnull)
        except:
            if sys.platform == 'win32':
                self.PATH_TO_STATUS = "C:/Program Files (x86)/VentSrv/ventrilo_status"
                try:
                    subprocess.call([self.PATH_TO_STATUS], stdout=devnull, stderr=devnull)
                except:
                    self.PATH_TO_STATUS = "C:/Program Files/VentSrv/ventrilo_status"
                    try:
                        subprocess.call([self.PATH_TO_STATUS], stdout=devnull, stderr=devnull)
                    except:
                        print("Could not find ventrilo_status. Please set it in the script")
                        exit()
            else:
                print("Could not find ventrilo_status. Please set it in the script")
                exit()
        return

    def _get_simple_status(self):
        """
        Get the simple status of the server (-c1)

        :rtype: byte
        :return: The result from the status executable in byte form
        """
        COMMANDS_TO_APPEND_STATUS = "-c1"  # Any arguments to pass

        # Format a list so we can pass it to subprocess
        holder = [self.PATH_TO_STATUS]
        holder.append('-t' + self.server)
        for i in re.split(',| ', COMMANDS_TO_APPEND_STATUS):
            holder.append(i)

        return subprocess.check_output(holder)

    def _get_simple_status_dict(self):
        """
        Get the simple status of the server in dictionary form (-c1)

        :rtype: dict || None
        :return: A dictionary of the server info or None if it could not connect
        """
        count = 0
        while count < 4:
            output = self._get_simple_status()
            if "ERROR" in str(output):
                count = count + 1
                if self.verbose > 0:
                    print(
                        "No response from server while trying to get info, it may be down or your connection is down." +
                        " Trying again in 5 seconds. try " + str(count) + "/3")
                time.sleep(5)
                if count == 3:
                    return None
            else:
                break
        return self._get_dict_from_output(output)

    def _get_detailed_status(self):
        """
        Get the detailed status of the server (-c2)

        :rtype: byte
        :return: The result from the status executable in byte form
        """
        COMMANDS_TO_APPEND_STATUS = "-c2"  # Any arguments to pass

        # Format a list so we can pass it to subprocess
        holder = [self.PATH_TO_STATUS]
        holder.append('-t' + self.server)
        for i in re.split(',| ', COMMANDS_TO_APPEND_STATUS):
            holder.append(i)
        return subprocess.check_output(holder)

    def _get_detailed_status_dict(self):
        """
        Get the detailed status of the server in string form (-c2)

        :rtype: dict || None
        :return: A dictionary of the detailed server status output or None if it could not connect
        """
        count = 0
        while count < 4:
            output = self._get_detailed_status()
            if "ERROR" in str(output):
                count = count + 1
                if self.verbose > 0:
                    print(
                        "No response from server while trying to get info, it may be down or your connection is down." +
                        " Trying again in 5 seconds. try " + str(count) + "/3")
                time.sleep(5)
                if count == 3:
                    return None
            else:
                break
        return self._get_dict_from_output(output)

    def _format_string(self, output):
        """
        Format the output from the status command to a list

        :type output: byte
        :param output: The output from the status command
        :return: A list containing all the values from the status command
        """
        # Format the string to a usable state
        if sys.version_info < (3, 0):
            if sys.platform == 'win32':
                holder = str(output).split('\r\n')
            elif sys.platform.startswith('linux'):
                holder = str(output).split('\n')
            elif sys.platform == 'darwin':
                holder = str(output).split('\r')
        else:
            holder = str(output)
            holder = holder.replace("b'", '')
            holder = holder.replace('b"', '')
            if sys.platform == 'win32':
                holder = re.split(r'\\r\\n', holder)
            elif sys.platform == 'linux':
                holder = re.split(r'\\n', holder)
            elif sys.platform == 'darwin':
                holder = re.split(r'\\r', holder)

        holder.pop()  # Pop the last item because it's a apostrophe
        return holder

    def _get_dict_from_output(self, output):
        """
        formats the string from get_status

        :type output: byte
        :param output: The output from get_status
        :rtype: dict
        :return: A dictionary with the keys and values. 'CLIENT' and 'CHANNEL' are both dictionaries themselves
        """
        holder = self._format_string(output)
        # Make a dictionary off of the list made
        dic = {}
        key = []
        val = []
        for i in holder:
            loc = i.index(': ')
            key.append(i[:loc])
            val.append(i[loc+2:])

        for i in range(len(key)):
            if key[i - 1] == 'CLIENT':
                if dic.get('CLIENT') is None:
                    dic['CLIENT'] = {}
                client_dic = {}
                client_info = val[i - 1].split(',')
                for info in client_info:
                    key_value_pair = info.split('=')
                    client_dic[key_value_pair[0]] = key_value_pair[1]
                dic['CLIENT'][client_dic['NAME']] = client_dic
            elif key[i - 1] == 'CHANNEL':
                if dic.get('CHANNEL') is None:
                    dic['CHANNEL'] = {}
                client_dic = {}
                client_info = val[i - 1].split(',')
                for info in client_info:
                    key_value_pair = info.split('=')
                    client_dic[key_value_pair[0]] = key_value_pair[1]
                dic['CHANNEL'][client_dic['NAME']] = client_dic
            else:
                dic[key[i - 1]] = val[i - 1]
        return dic

##----------- User functions ------------
    def update_simple(self):
        """
        Update the output to the simple status of the server

        :rtype: None
        :return: None
        """
        self.status = self._get_simple_status_dict()
        return

    def update_detailed(self):
        """
        Update the class status to the detailed status of the server

        :rtype: None
        :return: None
        """
        self.status = self._get_detailed_status_dict()
        return

    def check_if_server_is_valid(self):
        """
        Determine if the server is responsive
        is_server_alive will be true if it can connect, and a string of the problem otherwise.

        :rtype: bool
        :return: If the server is seekable
        """
        try:
            output = self._get_simple_status()
        except:
            self.is_server_alive =  ["Unable to resolve hostname " + self.server + ", check your input", 'resolveError']
            return False
        if "ERROR" in str(output):
            self.is_server_alive = ["No response from server " + self.server +
                                    ", it may be down or your connection is down", 'noResponse']
            return False
        self.is_server_alive = True
        return True

    def get_user_count(self):
        """
        Get the number of users in the server

        :rtype: int
        :return: the number of clients connected to the server
        """
        STRING_TO_LOOK_FOR = 'CLIENTCOUNT'

        # If the key does not exist in the status, update it
        if not (STRING_TO_LOOK_FOR in self.status):
            self.update_simple()
        return int(self.status[STRING_TO_LOOK_FOR])

    def get_server_version(self):
        """
        Get the version of the server

        :rtype: str
        :return: the version of the server
        """
        STRING_TO_LOOK_FOR = 'VERSION'

        # If the key does not exist in the status, update it
        if not (STRING_TO_LOOK_FOR in self.status):
            self.update_detailed()
        return self.status[STRING_TO_LOOK_FOR]

    def get_client_list(self, should_be_string=False):
        """
        Get the list of all connected clients in either a list or a string

        :type should_be_string: bool
        :param should_be_string: A flag to say to return a string instead of a list
        :rtype: list || str
        :return: A list of users, or a string if the flag is set
        """
        STRING_TO_LOOK_FOR = 'CLIENT'

        if self.get_user_count() == 0:
            if should_be_string:
                return ''
            else:
                return []

        # If the key does not exist in the status, update it
        if not (STRING_TO_LOOK_FOR in self.status):
            self.update_detailed()

        if should_be_string == True:
            return ', '.join([out for out in self.status[STRING_TO_LOOK_FOR].keys()])
        else:
            return self.status[STRING_TO_LOOK_FOR].keys()

    def does_client_exist_on_server(self, user):
        """
        Does the user exist on the server?

        :type user: str
        :param user: The user to look for
        :rtype: bool
        :return: True if the user exists in the server, False otherwise
        """
        STRING_TO_LOOK_FOR = 'CLIENT'

        #If there are no users in the server
        if self.get_user_count() == 0:
            return False

        # If the key does not exist in the status, update it
        if not (STRING_TO_LOOK_FOR in self.status):
            self.update_detailed()

        return user in self.status[STRING_TO_LOOK_FOR]

    def get_client_comment(self, user):
        """
        Get the comment of the user

        :type user: str
        :param user: The user to check
        :rtype: str
        :return: String of the clients comment
        """

        #If there are no users in the server
        if self.get_user_count() == 0:
            return False

        # If the key does not exist in the status, update it
        if not ('CLIENT' in self.status):
            self.update_detailed()

        #If the COMM keyword does not exist, the server has this disabled.
        if not 'COMM' in self.status['CLIENT'][list(self.status['CLIENT'].keys())[0]]:
            print("Server does not support comments. Log in as server admin " +
                  "and enable this under server properties or contact the server maintainer")
            raise KeyError("The comment key does not exist in the server output. It must be enabled by a server admin.")
        else:
            return self.status['CLIENT'][user]['COMM']


def check_if_server_is_valid(server):
    """
    Determine if the server is responsive

    :rtype: bool || str
    :return: True if the server is seekable or a string of the error if not
    """

    checker = VentriloServerInfo(server)
    try:
        output = checker._get_simple_status()
    except:
        print("Unable to resolve hostname " + server + ", check your input", 'resolveError')
        return 'resolveError'
    if "ERROR" in str(output):
        print("No response from server " + server +
                                ", it may be down or your connection is down", 'noResponse')
        return 'noResponse'
    return True


if __name__ == '__main__':
    """A simple test"""
    servers_to_try = ['LoE.DarkStarVent.Com:11332', 'vent.publicvent.com:4211', 'tan.cleanvoice.ru:3808']

    for server in servers_to_try:
        serverInfo = VentriloServerInfo(server)
        if serverInfo.is_server_alive != True:
            print(server + " was not reachable" + os.linesep)
            continue
        serverInfo.update_simple()
        print(serverInfo.status['NAME'] + " Has " + str(serverInfo.get_user_count()) + " users out of " +
              str(serverInfo.status['MAXCLIENTS']))
        days = int(serverInfo.status['UPTIME']) / 86400
        print("And has been up for " + str(days) + " days" + os.linesep)