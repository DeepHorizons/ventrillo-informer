#!/usr/bin/env python
"""
An interface to multiple music clients for simple control

Works with:
Winamp through winamp.py
mpd2 through python-mpd2 https://github.com/Mic92/python-mpd2
musicpd through python-musicpd https://pypi.python.org/pypi/python-musicpd

Author: Joshua Milas
Python: 3.4.1
Version: 0.1
Last Modified: 2014-08-09
"""

import sys
import time

#use monotomic time if possible
if sys.version_info > (3, 3):
    time.time = time.monotonic

#Supported clients
WINAMP = 'winamp'
MPD2 = 'mpd'
MUSICPD = 'musicpd'

#Playback status
STOPPED = 'stopped'
PLAYING = 'playing'
PAUSED = 'paused'

#The statuses of a mpd server
MPD_STATUS = {'play': PLAYING,
              'stop': STOPPED,
              'pause': PAUSED}

#What is the timeout of the server. 60 seconds is the default
MPD_TIMEOUT = 60


class Music(object):
    def __init__(self, player=None, server='', port=6600):
        """
        Init the music interface.

        If no player is specified, it will automatically try to select the proper player

        :type player: str
        :param player: The player to select if manually selecting. Use the constants provided
        :type server: str
        :param server: The server to connect to. Only applicable for mpd2 or musicpd.
        :type port: int
        :param port: The port to use. Only applicable for mpd2 or musicpd.
        :rtype: None
        :return: None
        """
        if player is None:
            try:
                import winamp
                if not winamp.doesWinampExist():
                    raise ImportError
                self._player = winamp.Winamp()
                self._whatplayer = WINAMP
            except ImportError:
                try:
                    import mpd
                    self._player = mpd.MPDClient()
                    self._whatplayer = MPD2
                    self._server = server
                    self._port = port
                    self._connect()
                except ImportError:
                    try:
                        import musicpd
                        self._player = musicpd.MPDClient()
                        self._whatplayer = MUSICPD
                        self._server = server
                        self._port = port
                        self._connect()
                    except ImportError:
                        print("Automatic check: You do not have any any supported " +
                              "music players or they are not installed")
        elif player.lower() == WINAMP:
            import winamp
            self._player = winamp.Winamp()
            self._whatplayer = WINAMP
        elif player.lower() == MPD2:
            import mpd
            self._player = mpd.MPDClient()
            self._whatplayer = MPD2
            self._server = server
            self._port = port
            self._connect()
        elif player.lower() == MUSICPD:
            import musicpd
            self._player = musicpd.MPDClient()
            self._whatplayer = MUSICPD
            self._server = server
            self._port = port
            self._connect()
        return

    def _connect(self):
        """
        For connecting to a remote server
        Used for mpd2 and musicpd

        :rtype: None
        :return: None
        """
        if self._whatplayer != MPD2 and self._whatplayer != MUSICPD:
            return

        if self._server == '':
            self._server = '127.0.0.1'
            try:
                self._player.connect(self._server, self._port)
            except ConnectionRefusedError:
                import socket
                listofaddresses = [socket.gethostbyname(socket.gethostname())]
                if '127.' in listofaddresses[0]:
                    listofaddresses = []
                    for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]:
                        s.connect(('8.8.8.8', 80))
                        listofaddresses.append(s.getsockname()[0])
                        s.close()
                didconnect = False
                for ip in listofaddresses:
                    try:
                        self._player.connect(self._server, self._port)
                        didconnect = True
                        self._server = ip
                        break
                    except Exception:
                        pass
                if not didconnect:
                    raise ConnectionRefusedError("Was unable to connect to server. " + self._server + ':' +
                                            str(self._port) + " . Check that it is up or that you typed it in right")
        else:
            if self._whatplayer == MPD2 or self._whatplayer == MUSICPD:
                try:
                    self._player.connect(self._server, self._port)
                except Exception:
                    raise ConnectionRefusedError("Was unable to connect to server " + self._server + ':' +
                                                 str(self._port) +  " . Check that it is up")
        self.lastcommandtime = time.time()
        return

    def _disconnect(self):
        """
        Disconnect from the server
        Used for mpd2 and musicpd

        :rtype: None
        :return: None
        """
        if self._whatplayer != MPD2 and self._whatplayer != MUSICPD:
            return

        self._player.disconnect()
        return

    def checkconnection(self):
        """
        Make sure the mpd client is connected

        Since the mpd server has a timeout for idle clients, and it would be bad to set the timeout to infinity...

        :rtype: None
        :return: None
        """
        if self._whatplayer != MPD2 and self._whatplayer != MUSICPD:
            return

        try:
            self._player.connect(self._server, self._port)
        except Exception:
            self._disconnect()
            self._connect()
        return

    def play(self):
        """
        Start playing music

        :rtype: None
        :return: None
        """
        self.checkconnection()
        self._player.play()
        return

    def stop(self):
        """
        Stop playing music

        :rtype: None
        :return: None
        """
        self.checkconnection()
        self._player.stop()
        return

    def next(self):
        """
        Go to the next song

        :rtype: None
        :return: None
        """
        self.checkconnection()
        self._player.next()
        return

    def previous(self):
        """
        Go to the previous song

        :rtype: None
        :return: None
        """
        self.checkconnection()
        self._player.previous()
        return

    def setvol(self, vol):
        """
        Set the volume

        vol is 0%-100%

        :type vol: int
        :param vol: The volume to set the player to
        :rtype: None
        :return: None
        """
        if vol < 0:
            vol = 0
        if vol > 100:
            vol = 100

        if self._whatplayer == WINAMP:
            self._player.setVolume(vol*(255/100))
        elif self._whatplayer == MPD2 or self._whatplayer == MUSICPD:
            self.checkconnection()
            self._player.setvol(vol)
        return

    def getplaybackstatus(self):
        """
        Gets the playback status of the player

        :rtype: int
        :return: The playback status as defined by PLAYBACK
        """
        if self._whatplayer == WINAMP:
            status = self._player.getPlaybackStatus()
            if status == self._player.PLAYBACK_NOT_PLAYING:
                return STOPPED
            elif status == self._player.PLAYBACK_PLAYING:
                return PLAYING
            elif status == self._player.PLAYBACK_PAUSE:
                return PAUSED
        elif self._whatplayer == MPD2 or self._whatplayer == MUSICPD:
            self.checkconnection()
            status = self._player.status()
            status = status['state']
            return MPD_STATUS[status]

    def getsongname(self):
        """
        Gets the name of the current song playing

        :rtype: str
        :return: The name of the song playing, or "" if not playing
        """
        if self.getplaybackstatus() == STOPPED:
            return ""

        if self._whatplayer == WINAMP:
            return self._player.getCurrentPlayingTitle()
        if self._whatplayer == MPD2 or self._whatplayer == MUSICPD:
            self.checkconnection()
            return ['.'.join([song for song in self._player.currentsong()['file'].split('/')[-1].split('.')[:-1]])][0]