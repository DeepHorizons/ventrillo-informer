#!/usr/bin/env python
"""
A script to watch Ventrilo servers, look for specific people, and report via text if set to do so

Requires the ventrilo_status program available at http://www.ventrilo.com/download.php
It is included in the server package

Requires curl available at http://curl.haxx.se/download.html

Modify the constants to point to these binaries

Author: Joshua Milas
Python: 3.4.1
Version: 0.1
Last Modified: 2014-08-02
"""

import subprocess
import time
import datetime
import argparse
import os
import Ventrilo_server_info

__version__ = "0.1"

PATH_TO_CURL = 'curl'


def check_programs():
    """
    Try the program paths to make sure they work. Report if they cannot be found
    :return: Nothing
    """
    devnull = open(os.devnull, 'w')  # The null device so we do not print to the console
    global PATH_TO_CURL

    #Curl
    try:
        subprocess.call([PATH_TO_CURL], stdout=devnull, stderr=devnull)
    except:
        print("Could not find curl. Please set the path in the script")
        exit()
    return


def get_time_string():
    """
    Get the current date and time
    :return: The current date and time in string format
    """
    now = datetime.datetime.now()
    return " " + str(now.hour) + ":" + str(now.minute).rjust(2, '0') + ":" + str(now.second) + " "


def send_text(number, message):
    """
    The wrapper to send a text

    :param number: The number to text
    :param message: The message to send
    :return:
    """
    subprocess.call([PATH_TO_CURL, "http://textbelt.com/text",
                    '-d', "number=" + number, '-d', "message=" + message]); print(' ')
    return


#----------------------------------------------------------------------
def main(server, number="", people=[], verbose=0, can_use_partial=False):
    """
    the main function of the Ventrilo Server Query and texter

    :type server: str
    :param server: The server to connect to in server:port form

    :type number: str
    :param number: The number to text. If people is set, then it will text when only those people are in the server, \
                    otherwise it will text when there is anyone in the server
    :type people: list
    :param people: A list of strings of people to look for.

    :type verbose: int
    :param verbose: A verbose level. 0 is print nothing, 1 is print basic statistics, 2 is print all statistics

    :type can_use_partial: bool
    :param can_use_partial: Whether to lookup the exact username, or a partial match will suffice
    :return: None
    """
    #TODO better time management and a way to pass it from the command line
    days_to_restrict_time = [1, 2, 3, 4, 5]  # Days that you want the time set below to take effect, set to weekdays
    earliest = datetime.time(hour=17)
    latest = datetime.time(hour=23, minute=59)
    days_for_open_time = [6, 7]  # Days where you don't care about the time, set to the weekend

    checker = Ventrilo_server_info.VentriloServerInfo(server, verbose)

    has_texted = False
    is_outside_bounded_time = False

    while True:
        now = datetime.datetime.now().time()
        day = datetime.datetime.today().isoweekday()

        #Is the time within the boundaries?
        if (day in days_to_restrict_time and earliest < now < latest) \
                or day in days_for_open_time:
            is_outside_bounded_time = False
            #Get server query output and user count
            checker.update_detailed()
            if checker.status is None:
                while checker.check_if_server_is_valid() is False:
                    if verbose > 0:
                        print(checker.is_server_alive[0])
                        print("Waiting 60 seconds for next check")
                    time.sleep(60)
                continue

            user_count = checker.get_user_count()

            #Print statistics
            if verbose > 0:
                print("----------" + get_time_string() + "----------")
                print('Server: ' + checker.server)
                if verbose > 1:
                    print("Server Version: " + checker.get_server_version())
                print("User Count: " + str(user_count) + "\n")

            if user_count > 0:
                text_string = "1"
                #If there is a number...
                if number:
                    #...but no people to check for
                    if not people:
                        text_string = "vent: There are " + str(user_count) +\
                                      " users connected to '" + server + "'. Users Connected:"
                    else:
                        #Otherwise we are looking for people
                        text_string = "vent: " + server + " . People in the server are:"

                if verbose > 1:
                    print("Connected Users:")
                for i in checker.get_client_list():
                    if verbose > 1:
                        print(i)
                    if number:
                        text_string += "\n" + i

                for person in people:
                    if (can_use_partial and person.lower() in checker.get_client_list(True).lower()) \
                            or (person in checker.get_client_list()):
                        if verbose > 1 or not people:
                            print(person + " exists as a client name in the server")
                    else:
                        text_string = ""

                if verbose > 0 and people and text_string:
                    print("---All requested people are in the server---")

                if has_texted is False and number != "" and text_string != "":
                    if verbose > 0:
                        print("Sending Text")
                    has_texted = True
                    send_text(number, text_string)

                if verbose > 1:
                    print("Waiting 60 seconds until next query")
                time.sleep(60)

            else:
                if verbose > 1:
                    print("No users, waiting 60 seconds")
                has_texted = False
                time.sleep(60)

        else:
            if is_outside_bounded_time is False:
                if(verbose > 0):
                    print("Not within bounded time. Waiting until within time frame")
                has_texted = False
                is_outside_bounded_time = True
            time.sleep(60)
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    Query a Ventrilo server and get a list of users in it.
    If a number is set, it will text you when it detects someone in the server.
    If the people flag is set, it will only text you when those people are in the server.
    Can only text US numbers.

    Requires curl in order to run,
    and is available at http://curl.haxx.se/download.html

    Requires the ventrilo_status program available at
    http://www.ventrilo.com/download.php and it is included in the server package.

    Modify the script to point to to correct binaries''',
                                     usage=os.path.basename(__file__) + ''' server [-n NUMBER] [-p "PEOPLE"] [-e]
                                     [-f] [-v | -vv]''',
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('server', help="The server to connect to in server:port form")
    parser.add_argument('-n', '--number', default="", help="The number to text. Example: 1234567890, \
                                    (123)456-7890, etc.")
    parser.add_argument('-p', '--people', default=[], help='''Users to look for. If these users are in the server,
                                    the script will text you. Users must be a comma separated list and
                                    surrounded by commas. Example: \"testUser,Another User,YetOneMore11\"''')
    parser.add_argument('-e', '--exact', action='store_true', default=False, help='''Would you like the exact user name
                                    to appear, or do you    you only need a part to match (eXamPleUs3r, with -e, you
                                    only need -p "example" to find them, without -e, you need -p "eXamPleUs3r"''')
    parser.add_argument('-v', '--verbose', action='count', default=0, help='''Increase output verbosity. -v is all
                                    requested info, -vv is all info, and no flag is no output''')
    parser.add_argument('-f', '--force', action='store_true', default=False, help='''Force connecting to a server even
                                    if there is no response from it. Does not include unresolved hostnames''')

    args = parser.parse_args()

    check_programs()
    server_check = Ventrilo_server_info.check_if_server_is_valid(args.server)
    if server_check != True:
        if server_check == 'resolveError':
            print("Could not resolve the hostname of the server.")
            exit()
        elif server_check == 'noResponse' and args.force == False:
            print("There was no response from the server. Apply the -f flag to ignore this error")
            exit()


    number = args.number
    people = args.people

    #TODO parse the number better
    #Remove all spaces, dashes, and pluses
    if args.number != "":
        number = number.replace(' ', '')
        number = number.replace('-', '')
        number = number.replace('+', '')
        number = number.replace('(', '')
        number = number.replace(')', '')

    if number != "" and len(number) != 10:
        print("Incorrect number, must enter 10 digit number")
        exit()

    if people != []:
        people = people.split(',')

    print("Will connect to server: " + args.server)
    if number == "":
        print("No number set, will not text")
    else:
        print("Will text: " + "(" + number[:3] + ")" + number[3:6] + '-' + number[6:])
        if args.people == "":
            print("No people set, will text when anyone is in the server")

    if args.people == []:
        print("Not Looking for anyone")
    else:
        temp = ""
        for i in people:
            temp += i + "; "
        else:
            print("Looking for: " + temp)

    print("Verbose level: " + str(args.verbose))

    main(args.server, number, people, args.verbose, args.exact)