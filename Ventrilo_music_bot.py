#!/usr/bin/env python
"""
A Ventrilo music bot

Author: Joshua Milas
Python: 3.4.1
Version: 0.1
Last Modified: 2014-08-02
"""

import Ventrilo_client_manipulator
import Ventrilo_server_info
import music
import time
import re
import sys

#Use perf_counter if it exists
if sys.version_info >= (3, 3):
    time.clock = time.perf_counter

NAME_OF_BOT = 'MusicBot'
SERVER = '127.0.0.1:3784'

COME = ['come', "Tell the bot to join the server"]
STOP = ['stop', 'stp', "Stop playing music"]
BYE = ['bye', 'leave', "Tell the bot to leave the server"]
PLAY = ['play', 'ply', 'start', "Start playing music"]
VOLUME = ['volume', 'vol', "Adjust the volume, vol [0 - 100]"]
NEXT = ['next', 'nxt', "Play the next song"]
PREV = ['prev', 'prv', 'previous', 'back', "Go back one song"]
COMMANDS = ['commands', "List all available commands"]

HELP = ['help', "Do 'help <command>' to get help on a specific command, do 'commands' to get a list of commands"]


ALLCOMMANDS = [COME, STOP, BYE, PLAY, VOLUME, NEXT, PREV, COMMANDS, HELP]

bot_commands = {HELP[0]: {}}


def contains_parameter(to_check_for, list_to_check_against):
    for i in to_check_for:
        for j in list_to_check_against:
            if isinstance(j, tuple):
                if i in j[0]:
                    return True
            else:
                if i in j:
                    return True
    return False


def get_index(to_check_for, list_to_check_against):
    for i in to_check_for:
        for j in range(len(list_to_check_against)):
            if type(list_to_check_against[j]) is type(tuple):
                if i in list_to_check_against[j]:
                    return j
            else:
                if i in list_to_check_against[j]:
                    return j
    return None


def add_to_dict(dic, lst):
    for i in range(len(lst)-1):
        if lst[i] != HELP[0]:
            dic[lst[i]] = lst[0]
        dic[HELP[0]][lst[i]] = lst[-1]

if __name__ == '__main__':
    MAX_USERS = 4
    TIME_BEFORE_NEXT_STATUS_UPDATE = 5
    TIME_BEFORE_NEXT_COMMENT_UPDATE = 8

    for command in ALLCOMMANDS:
        add_to_dict(bot_commands, command)

    checker = Ventrilo_server_info.VentriloServerInfo(SERVER, 2)
    vent = Ventrilo_client_manipulator.VentriloWindow(NAME_OF_BOT)
    player = music.Music()

    while True:

        #wait until we are summoned
        while True:
            can_join = False
            checker.update_detailed()
            for person in checker.get_client_list():
                if 'music:' in checker.get_client_comment(person).lower():
                    comment = re.split(r'music:| ', checker.get_client_comment(person).lower())
                    if contains_parameter(COME, comment):
                        print('can come')
                        can_join = True
            if can_join:
                break
            time.sleep(TIME_BEFORE_NEXT_STATUS_UPDATE)

        #If we are below the client level, dont play any music to start and just connect
        if (checker.get_user_count() <= MAX_USERS - 1) and not vent.is_connected:
            player.stop()
            vent.select_user()
            vent.select_server(2)
            vent.connect()
            vent.write_comment("Put 'music: play' in comment to start music")

        comment_tracker = 0
        last_changed_comment = time.clock()
        last_updated_status = time.clock()

        while checker.get_user_count() <= MAX_USERS:
            if time.clock() - last_updated_status > TIME_BEFORE_NEXT_STATUS_UPDATE:
                last_updated_status = time.clock()
                checker.update_detailed()
                command_list = []

                for person in checker.get_client_list():
                    if person == vent.name:
                        continue
                    if 'music:' in checker.get_client_comment(person).lower():
                        comment = re.split(r'music:| ', checker.get_client_comment(person).lower())
                        comment = [x for x in comment if x]  # Remove all spaces
                        print(comment)
                        if not comment[0] in bot_commands:
                            vent.write_comment(person + ": Bad Command")
                            continue

                        if contains_parameter(HELP, comment):   # Need this one because help contains all the commands
                            if len(comment) > 1:
                                command_list.append((HELP[0], person, comment[1]))
                            else:
                                command_list.append((HELP[0], person))
                        elif comment[0] in bot_commands:
                            if len(comment) > 1:
                                command_list.append((bot_commands[comment[0]], person, comment[1]))
                            else:
                                command_list.append((bot_commands[comment[0]], person))

                #TODO Handle multiple users simultaneously without starvation or conflicts
                if contains_parameter(STOP, command_list):
                    if player.getplaybackstatus() != music.STOPPED:
                        player.stop()
                        print('stopping music')
                elif contains_parameter(BYE, command_list):
                    print('leaving')
                    break
                else:
                    if contains_parameter(PLAY, command_list):
                        if player.getplaybackstatus() != music.PLAYING:
                            print('playing')
                            player.play()
                    if contains_parameter(VOLUME, command_list):
                        index = get_index(VOLUME, command_list)
                        try:
                            vol = int(command_list[index][-1])
                        except ValueError:
                            vent.write_comment(command_list[index][1] + ": Not a valid volume")
                        else:
                            if vol < 0:
                                vol = 0
                            if vol > 100:
                                vol = 100

                            print("setting vol")
                            player.setvol(vol)
                    if contains_parameter(NEXT, command_list):
                        print('next')
                        player.next()
                    if contains_parameter(PREV, command_list):
                        print('prev')
                        player.previous()
                    if contains_parameter(HELP, command_list):
                        index = get_index(HELP, command_list)
                        cmd = command_list[index]
                        if len(cmd) > 2:
                            vent.write_comment(bot_commands[HELP[0]][cmd[-1]])
                            last_changed_comment = time.clock()
                        else:
                            vent.write_comment(bot_commands[HELP[0]][cmd[0]])
                            last_changed_comment = time.clock()
                    if contains_parameter(COMMANDS, command_list):
                        vent.write_comment(' '.join(bot_commands.keys()))
                        last_changed_comment = time.clock()

            if time.clock() - last_changed_comment > TIME_BEFORE_NEXT_COMMENT_UPDATE:
                last_changed_comment = time.clock()
                if player.getplaybackstatus() is music.PLAYING:
                    if comment_tracker == 0:
                        comment_tracker = 1
                        vent.write_comment("Put 'music: stop' to stop music")
                    elif comment_tracker == 1:
                        comment_tracker = 0
                        #TODO this doesnt seem to work in version 2.x
                        vent.write_comment("Current Song: " + player.getsongname())
                if player.getplaybackstatus() is music.STOPPED:
                    if comment_tracker == 0:
                        comment_tracker = 1
                        vent.write_comment("Put 'music: play' in comment to start music")
                    elif comment_tracker == 1:
                        comment_tracker = 0
                        vent.write_comment("Josh is the best!")

        #disconnect
        vent.disconnect()