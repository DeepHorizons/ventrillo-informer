#!/usr/bin/env python
"""
A script to manipulate a ventrilo client window
requires pywin32

Author: Joshua Milas
Python: 3.4.1
Version: 0.1
Last Modified: 2014-08-02
"""

import win32gui
import win32con
import time
import subprocess
import win32process

class VentriloWindow():
    COMMENTWAITTIME = .02

    def __init__(self, name="", path_to_vent='ventrilo'):
        self.name = name
        self.PATH_TO_VENTRILO = path_to_vent

        self._find_ventrilo()  # Make sure we got the correct executable
        self.process = self._create_client()  # Create the window
        self.id = self.get_ventrilo_id(self.process.pid)
        self.minimize_window()
        self.is_connected = False
        return

    def _find_ventrilo(self):
        """
        Find the Ventrilo executable and exit if it's not found

        :rtype: None
        :return: None
        """
        import os, sys
        devnull = open(os.devnull, 'w')  # The null device so we do not print to the console

        try:
            test_id = subprocess.Popen([self.PATH_TO_VENTRILO, '-m'], stdout=devnull, stderr=devnull)
        except:
            if sys.platform == 'win32':
                #Try the windows executable defaults
                self.PATH_TO_VENTRILO = "C:/Program Files/Ventrilo/Ventrilo"
                try:
                    test_id = subprocess.Popen([self.PATH_TO_VENTRILO, '-m'], stdout=devnull, stderr=devnull)
                except:
                    self.PATH_TO_VENTRILO = "C:/Program Files (x86)/Ventrilo/Ventrilo"
                    try:
                        test_id = subprocess.Popen([self.PATH_TO_VENTRILO, '-m'], stdout=devnull, stderr=devnull)
                    except:
                        print('Could not find')
                        exit()
            else:
                print("Could not find the Ventrilo client program")
                exit()
        test_id.terminate()
        return

    def _create_client(self):
        """
        Create a ventrilo client window

        :rtype: None
        :return: None
        """
        return subprocess.Popen([self.PATH_TO_VENTRILO, '-m'])

    def close_client(self):
        """
        Close the client window

        :rtype: None
        :return: None
        """
        self.process.terminate()
        return

    def get_ventrilo_id(self, pid):
        """
        Get the id of the window so pywin32 can manipulate it

        :type pid: int
        :param pid: The PID of the process
        :rtype: int
        :return: The winAPI hardware handle
        """
        def callback(hwnd, hwnds):
            if win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
                _, found_pid = win32process.GetWindowThreadProcessId(hwnd)
                if found_pid == pid:
                    hwnds.append(hwnd)
            return True
        hwnds = []

        #Wait for the window to be created
        while len(hwnds) < 1:
            win32gui.EnumWindows(callback, hwnds)
        return hwnds[0]

    def set_ventrilo_to_foreground(self):
        """
        Bring the client to the foreground

        :rtype: None
        :return: None
        """
        win32gui.ShowWindow(self.id, win32con.SW_RESTORE)
        win32gui.SetWindowPos(self.id, win32con.HWND_NOTOPMOST, 0, 0, 0, 0, win32con.SWP_NOMOVE + win32con.SWP_NOSIZE)
        win32gui.SetWindowPos(self.id, win32con.HWND_TOPMOST, 0, 0, 0, 0, win32con.SWP_NOMOVE + win32con.SWP_NOSIZE)
        win32gui.SetWindowPos(self.id, win32con.HWND_NOTOPMOST, 0, 0, 0, 0, win32con.SWP_SHOWWINDOW + win32con.SWP_NOMOVE + win32con.SWP_NOSIZE)
        time.sleep(.3)
        return

    def minimize_window(self):
        """
        Minimize the client window

        :rtype: None
        :return: None
        """
        win32gui.ShowWindow(self.id, win32con.SW_MINIMIZE)
        return

    def select_user(self, user=''):
        """
        Select the user in the client window. If user is an int, then it is a 1 indexed selection. If user is a
        string, then that user is looked for in the list. If left blank, then the name will be used
        An error is raised if the user cannot be found

        :type user: int
        :param user: If an int, a one index of the user, if a string, look for that user
        :rtype: None
        :return: None
        """
        user_name_box = win32gui.FindWindowEx(self.id, None, 'ComboBox', None)
        win32gui.SendMessage(user_name_box, win32con.CB_SHOWDROPDOWN, 1, 0)
        if type(user) is int:
            returnVal = win32gui.SendMessage(user_name_box, win32con.CB_SETCURSEL, user-1, 0)
        elif user != '':
            returnVal = win32gui.SendMessage(user_name_box, win32con.CB_SELECTSTRING, -1, user)
        else:
            returnVal = win32gui.SendMessage(user_name_box, win32con.CB_SELECTSTRING, -1, self.name)

        if returnVal == win32con.CB_ERR:
            print("Unable to find user")
            raise ValueError("Index out of range or string not found")
        win32gui.SendMessage(user_name_box, win32con.WM_LBUTTONDOWN, 0, -1)
        win32gui.SendMessage(user_name_box, win32con.WM_LBUTTONUP, 0, -1)
        time.sleep(.1)
        return

    def select_server(self, server=1):
        """
        Select the server in the client window. If server is an int, then it is a 1 indexed selection. If server is a
        string, then that server is looked for in the list. An error is rased if the user cannot be found

        :type server: int str
        :param server: If an int, a one index of the user, if a string, look for that user
        :rtype: None
        :return: None
        """
        server_box = win32gui.FindWindowEx(self.id, None, 'ComboBox', None)
        server_box = win32gui.FindWindowEx(self.id, server_box, 'ComboBox', None)   # It's the second combo box
        win32gui.SendMessage(server_box, win32con.CB_SHOWDROPDOWN, 1, 0)
        if type(server) is int:
            returnVal = win32gui.SendMessage(server_box, win32con.CB_SETCURSEL, server-1)
        else:
            returnVal = win32gui.SendMessage(server_box, win32con.CB_SELECTSTRING, -1, server)

        if returnVal == win32con.CB_ERR:
            print("Unable to find server")
            raise ValueError("Index out of range or string not found")
        win32gui.SendMessage(server_box, win32con.WM_LBUTTONDOWN, 0, -1)
        win32gui.SendMessage(server_box, win32con.WM_LBUTTONUP, 0, -1)
        time.sleep(.1)
        return

    def connect(self):
        """
        Connect to the server if not already connected

        :rtype: None
        :return: None
        """
        connect_button = win32gui.FindWindowEx(self.id, None, 'Button', 'C&onnect')
        win32gui.PostMessage(connect_button, win32con.BM_CLICK, 0, 0)
        self.is_connected = True
        time.sleep(.3)
        return

    def disconnect(self):
        """
        Disconnect from the server if not already

        :rtype: None
        :return: None
        """
        disconnect_button = win32gui.FindWindowEx(self.id, None, 'Button', 'Disc&onnect')
        win32gui.PostMessage(disconnect_button, win32con.BM_CLICK, 0, 0)
        self.is_connected = False
        time.sleep(.2)
        return

    def click_comment(self):
        """
        Clicks the comment button

        :rtype: None
        :return: None
        """
        comment_button = win32gui.FindWindowEx(self.id, None, 'Button', 'Comment')
        win32gui.PostMessage(comment_button, win32con.BM_CLICK, 0, 0)
        return

    def write_comment(self, string_to_write):
        """
        Write a comment

        :type string_to_write: str
        :param string_to_write: The string to write as a comment
        :rtype: None
        :return: None
        """
        self.click_comment()
        comment_window = 0
        while comment_window == 0:
            comment_window = win32gui.FindWindowEx(None, None, '#32770', "Comment - Advanced ")
        time.sleep(self.COMMENTWAITTIME)  # Give it a little time to settle
        while win32gui.GetParent(comment_window) != self.id:
            comment_window = win32gui.FindWindowEx(None, comment_window, '#32770', "Comment - Advanced ")
        comment_box = win32gui.FindWindowEx(comment_window, None, 'Edit', None)
        win32gui.SendMessage(comment_box, win32con.EM_SETSEL, 0,-1)  # Selects all the text
        win32gui.SendMessage(comment_box, win32con.EM_REPLACESEL, 0, string_to_write)   # Replace the selected text
        ok_button = win32gui.FindWindowEx(comment_window, None, 'Button', 'Ok')
        win32gui.PostMessage(ok_button, win32con.BM_CLICK, 0, 0)
        return

    def scroll_comment(self, string_to_write, displaylength=30, displaytime=8):
        start = 0
        end = displaylength
        speed = (displaytime - 1)/(len(string_to_write) - displaylength)
        comment = string_to_write[start:end]
        self.write_comment(comment)
        time.sleep(.5 - self.COMMENTWAITTIME)

        while end < len(string_to_write):
            start = start + 1
            end = end + 1
            comment = string_to_write[start:end]
            self.write_comment(comment)
            time.sleep(speed - self.COMMENTWAITTIME)

        start = start + 1
        end = end + 1
        comment = string_to_write[start:end]
        self.write_comment(comment)
        time.sleep(.5 - self.COMMENTWAITTIME)
        return



if __name__ == '__main__':
    # Just a simple test
    vent = VentriloWindow('MusicBot')   # Create the handler, which also creates the
    #vent.select_user(2)    # Select the user as a index of the list
    vent.select_user()  # Select the user as a search for this name
    vent.select_server(2)   # Select the server as an index in the list
    vent.connect()  # Connect to the server
    #vent.minimize_window()  # Make sure it's minimized. It is by default
    vent.write_comment('test')  # Write this comment
    time.sleep(2)   # Wait a bit
    vent.write_comment("Another T3$t")  # You can also write special characters
    time.sleep(2)
    vent.disconnect()   # Disconnect from the server
    vent.close_client() # Close the client window